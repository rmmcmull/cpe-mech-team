'''
@file lab4.py

@brief @brief This code uses mcp9808 module to take temperature readings every second from the mcp9808 temperature sensor. The taken temperature data is then written to a .csv file on the nucleo. Data is taken until the user presses Ctrl+C, at which time the prgram cleanly exits with no errors.


@author Ryan McMullen and David Meyenberg

@copyright CPE/MECH TEAM

@date February 11, 2021
'''

import pyb
from pyb import I2C
from mcp9808 import mcp9808
import utime

## 12 bit resolution, internal channels
adcall = pyb.ADCAll(12, 0x70000) 

## use this as a reference for chip 
adcall.read_vref() 

## Sets up I2C connection on channel 1 as a Master
i2c = I2C(1, I2C.MASTER) 

## Initializes mcp9808 object with I2C object created above, at address 24
MCP = mcp9808(i2c, 24) 

## MCP.check()
try:
    # write to temperature.csv
	with open ("temperatures.csv", "w") as a_file:
        
        ## initialize i
		i = 0
        
        # infinite loop
		while(True):
             
            ## read the core temperature of the board
			int_temp = adcall.read_core_temp()
            
            ## read the ambient temperature in degrees C
			amb_temp = MCP.celsius()
             
            #  print for user with time, internal temperature, and ambient temperature
			print("Time: " + str(i) + ", Internal temperature: " + str(int_temp) + ", Ambient temperature: " + str(amb_temp))
            
            ## write to .csv
			a_file.write(str(i) + ", " + str(int_temp) + ", " + str(amb_temp) + "\n")
             
            # sleep for 60 seconds
			utime.sleep60
             
            ## incriment i in loop
			i += 1
except:
	pass
 		