'''
@file temperature_plotter.py

@brief this code reads temperature data taken from a .csv file on the nucleo 
       and plots it.
       
@author Ryan McMullen and David Meyenberg

@copyright CPE/MECH TEAM

@date February 9, 2021
'''


from csv import reader
from matplotlib import pyplot
import numpy

## Times for plotting
times = [0 for t in range(1000)] 
## Internal temperatures
int_temps = [0 for t in range(1000)]
## Ambient temperatures
amb_temps = [0 for t in range(1000)]

#Reads the time and temperature values as a comma-seperated value file
with open("temperatures.csv", 'r') as csv_read_obj:
    ## Reads comma seperated value files
    csv_reader = reader(csv_read_obj)
    
    ## Counter for indexing .csv file
    i = 0
    
    #for loop for as many rows as are in the csv file
    for row in csv_reader: 
        # loop indefinately
        if(len(row) > 0): 
      
            # read first column with time
            times[i] = row[0]  
            
            # read second column with internal temperatures
            int_temps[i] = float(row[1])
            
            # read third column with ambient temperatures
            amb_temps[i] = float(row[2])
            
            # incriment row which is being read
            i += 1 
# Remove unused spots in arrays
## Times that were actually read
plotTimes = times[:i]
## Internal temperatures that were actually read
plotIntTemps = int_temps[:i]
## Ambient temperatures that were actually read
plotAmbTemps = amb_temps[:i]


# Plots the time and voltage values using pyplot

## Plot with time versus internal temperatures with a red line and legend which indicate "internal temperature"
pyplot.plot(plotTimes, plotIntTemps, "r--", label = "Internal Temperatures")

## Plot with time versus internal temperatures with a green line and legend which indicate "external temperature"
pyplot.plot(plotTimes, plotAmbTemps, "g--", label = "Ambient Temperatures")

## x axis label with time and units of minute
pyplot.xlabel("time (minutes)")

## y axis label with temperature and units of celsius
pyplot.ylabel("temperature (C)") 

## title plot with "STM32 Internal Temperature and MCP9808 Ambient Temperature"
pyplot.title("STM32 Internal Temperature and MCP9808 Ambient Temperature")

##limit y axis to 20,30
pyplot.ylim([20, 30])

## number of tick marks along x axis
x_ticks = numpy.arange(0, 61, 5)

## enable xticks
pyplot.xticks(x_ticks)

## enable legend
pyplot.legend()

## display all open figures
pyplot.show()


