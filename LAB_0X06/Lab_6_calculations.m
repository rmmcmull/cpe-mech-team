%% House Keeping
%  =============

clear
close all
format short

%% Parametric Values
%  =================

r_m = 60/1000;             % [mm] radius of lever arm
l_r = 50/1000;             % [mm] length of push rod
r_b = 10.5/1000;           % [mm] radius of ball
r_g = 42/1000;             % [mm] vertical distance from u-joint to CG of Platform
l_p = 110/1000;            % [mm] horizontal distance from u-joint to push-rod pivot
r_p = 32.5/1000;           % [mm] vertical distance from U-joint to push-rod pivot
r_c = 50/1000;             % [mm] vertical distance from U-joint to Platform Surface
m_b = 30/1000;             % [g] mass of ball
m_p = 400/1000;            % [g]  mass of platform
I_p = 1.88*10^6/1000^3;    % [g*mm^2] moment of inertia of platform
I_b = 2/5*m_b*r_b^2;       % [g*mm^2] inertia of the ball
b = 10/1000;               % [(m*N*m*s)/rad] viscous friction at U-joint
g = 9.81;                  % [mm/s^2] acceleration due to gravity

%% Coupled 2nd-order EQ ---> Set of Uncoupled 1st-order Equations
%  ==============================================================

% define symbolic variables
syms theta theta_d x x_d T

% define inertia matrix 
M = [-(m_b*r_b^2+m_b*r_c*r_b+I_b)/r_b -(I_b*r_b+I_p*r_b+m_b*r_b^3+m_b*r_b*r_c^2+2*m_b*r_b^2*r_c+m_p*r_b*r_g^2+m_b*r_b*x^2)/r_b;
     -(m_b*r_b^2+I_b)/r_b -(m_b*r_b^3+m_b*r_c*r_b^2+I_b*r_b)/r_b];
 
% define force matrix
f = [b*theta_d-g*m_b*(sin(theta)*(r_b+r_c)+x*cos(theta))+T*l_p/r_m+2*m_b*theta_d*x*x_d-g*m_p*r_g*sin(theta);
     -m_b*r_b*x*theta_d^2-g*m_b*r_b*sin(theta)];

% acceleration matrix
q_ddot = M\f;

% matrix which represents the g function of which d/dt*x_statespace is a
% function of.
g = [x_d; theta_d; q_ddot(1); q_ddot(2)];

% compute the jacobian with respect to ball's position, angular postion of
% platform, velocity of ball, and angular velocity of platform.
A = jacobian(g,[x; theta; x_d; theta_d]);

% compute the jacobian with respect to only torque of the motor 
B = jacobian(g,T);

% set equilibrium point to be at which all variables are equal to 0 for
% both the A and B jacobian matrices
A = subs(A,[x, theta, x_d, theta_d, T],[0,0,0,0,0]);
B = subs(B,[x, theta, x_d, theta_d, T],[0,0,0,0,0]);

% convert to standard array
A = double(A);
B = double(B);

% identity matrix for C
C = eye(4);

% all zeros for D matrix
D = zeros(4,1);

%% Gain Calculation by Char. Polynomial Matching
%  =============================================

% define gain values
syms K1 K2 K3 K4 s

% put proportional gains into a matrix
K = [K1 K2 K3 K4];

% closed loop system expression
A_cl = simplify(A-B.*K);

% compute the characteristic polynomial for the system under closed loop
% feedback
P_cl = transpose(det(s*eye(length(A)) - A_cl));

% grab coefficients for use in polynomial matching
c = coeffs(P_cl, s);

% for the desired system model, we select values for wn and zeta
wn = 5;
zeta = 0.8;

% find transfer function and corresponding
sys = tf([wn^2],[1 2*zeta*wn wn^2], 1);
P = pole(sys);

% next two poles are selected to be at least 10 times larger than the real 
% component of the two poles associated with zeta and wn
P(length(P)+1) = real(P(1).*10);
P(length(P)+1) = real(P(1).*10);

% characteristic polynomial can be evaluated for the desired fourth-order
% system.
P_des = (s-P(1))*(s-P(2))*(s-P(3))*(s-P(4));

% can then extract the coefficients from the desired polynomial
P_des_coeff = transpose(sym2poly(P_des));

% equate coefficient to solve for K values.. solve the system of 4
% equations
eq1 = c(2) == P_des_coeff(2);
eq2 = c(3) == P_des_coeff(3);
eq3 = c(4) == P_des_coeff(4);
eq4 = c(5) == P_des_coeff(5);
[E,F] = equationsToMatrix([eq1, eq2, eq3, eq4], [K1, K2, K3, K4]);
X = linsolve(E,F);

%% System Objects
%  ==============

% create a real-valued state space model to convert our dynamic system
% model to state-space model form.
sys = ss(A,B,C,D);

%% Choose Configuration to Plot
%  ============================

% set equal to 1, the configuration you want to plot
openloop = 1;
closedloop = 0;

%% Run Simulation
% ==============
% Execute command which invokes the simulink file to run. Ensure that the
% file name is correct.

% initial conditions for simulation form: [x theta x_d theta_d]
init_cond = [0; 0; 0; 0];

% gain for closed loop configuration
gain = [-0.05 -0.02 -0.3 -0.2];

run_time = .4; % [s] run time for simulink simulation

results = sim('platform_blockdiagram'); 

% which system do you want to plot?

if openloop == 1
    %% Plot Results (Open Loop)
    % =========================
    % Generate Single Plot
    % Code by Hans Mayer
    % Description:
    % This code generates a single plot and prints as a .jpeg image. The image, once
    % inserted into Word must be sized to reflect the desired size of the image.
    % --------------------
    % Velocity Versus Time
    % Set Figure Name
    figurename = 'Question 3 - Part D'; % USER TO MODIFY
    figure('Name',figurename,'NumberTitle','off',...
           'units','inches') % generate figure - set inch units for size
        pos = get(gcf,'pos'); % default MATLAB position of figure on screen
        set(gcf,'pos',[pos(1)/2 pos(2)/2 4.5 4.5]); % sets figure position and size
        % coordinates for 'pos' [1 2 3 4] - all inch units
        % 1 = distance of bottom left corner from left hand edge of screen
        % 2 = distance of bottom left corner from bottom edge of screen
        % 3 = figure width
        % 4 = figure height
        % YOU MAY NEED TO ADJUST 1 & 2 DEPENDING ON YOUR MONITOR SIZE!
        set(gcf,'PaperPosition',[0 0 4.5 4.5]); % sets figure print size in inches
        % coordinates for 'PaperPosition' [1 2 3 4] - all inch units
        % 1 & 2 = set bottom left corner
        % 3 = figure width, 4 = figure height
        % NOTE: The actual “printed” figure will NOT be these dimensions (the
        % default for -jpeg is 96 dpi). However, if the physical size of the
        % image is set to the 3 & 4 dimensions (in inches) after importing into Word,
        % then the font size will appear at the correct scale!
        % (11 pt is default for this code – change it if necessary).
        set(gcf,'DefaultAxesFontName','Times','DefaultAxesFontSize',11);
        % sets font to Times New Roman, 11 pt font size
        plot(results.tout,results.simpout(:,1),'k-'); % position of ball
        hold on 
        plot(results.tout,results.simpout(:,2),'k--'); % angular position of ball
        hold on 
        plot(results.tout,results.simpout(:,3),'k.-'); % velocity of ball
        hold on
        plot(results.tout,results.simpout(:,4),'k*'); % angular velocity of ball
        legend('potision [m]','angular postion [rad]','velocity [m/s]','angular velocity [rad/s]');
        xlabel('time, {\it t} [s]'); % x-label first graph
        ylabel('dynamic response, {\it q} []'); % y-label first graph
        %ylim([0 0.002]);
    % print command generates a jpeg figure with figurename
    % figure will save in the 'current folder' shown in MATLAB filepath
    print(gcf,figurename,'-djpeg','-r600'); % resolution set to 600 dpi
    
elseif closedloop == 1
    %% Plot Results (Closed Loop)
    % ===========================
    % Generate Single Plot
    % Code by Hans Mayer
    % Description:
    % This code generates a single plot and prints as a .jpeg image. The image, once
    % inserted into Word must be sized to reflect the desired size of the image.
    % --------------------
    % Velocity Versus Time
    % Set Figure Name
    figurename = 'Question 4'; % USER TO MODIFY
    figure('Name',figurename,'NumberTitle','off',...
           'units','inches') % generate figure - set inch units for size
        pos = get(gcf,'pos'); % default MATLAB position of figure on screen
        set(gcf,'pos',[pos(1)/2 pos(2)/2 4.5 4.5]); % sets figure position and size
        % coordinates for 'pos' [1 2 3 4] - all inch units
        % 1 = distance of bottom left corner from left hand edge of screen
        % 2 = distance of bottom left corner from bottom edge of screen
        % 3 = figure width
        % 4 = figure height
        % YOU MAY NEED TO ADJUST 1 & 2 DEPENDING ON YOUR MONITOR SIZE!
        set(gcf,'PaperPosition',[0 0 4.5 4.5]); % sets figure print size in inches
        % coordinates for 'PaperPosition' [1 2 3 4] - all inch units
        % 1 & 2 = set bottom left corner
        % 3 = figure width, 4 = figure height
        % NOTE: The actual “printed” figure will NOT be these dimensions (the
        % default for -jpeg is 96 dpi). However, if the physical size of the
        % image is set to the 3 & 4 dimensions (in inches) after importing into Word,
        % then the font size will appear at the correct scale!
        % (11 pt is default for this code – change it if necessary).
        set(gcf,'DefaultAxesFontName','Times','DefaultAxesFontSize',11);
        % sets font to Times New Roman, 11 pt font size
        plot(results.tout,results.simpout1(:,1),'k-'); % position of ball
        hold on 
        plot(results.tout,results.simpout1(:,2),'k--'); % angular position of ball
        hold on 
        plot(results.tout,results.simpout1(:,3),'k.-'); % velocity of ball
        hold on
        plot(results.tout,results.simpout1(:,4),'k*'); % angular velocity of ball
        legend('potision [m]','angular postion [rad]','velocity [m/s]','angular velocity [rad/s]');
        xlabel('time, {\it t} [s]'); % x-label first graph
        ylabel('dynamic response, {\it q} []'); % y-label first graph
        %ylim([0 0.002]);
    % print command generates a jpeg figure with figurename
    % figure will save in the 'current folder' shown in MATLAB filepath
    print(gcf,figurename,'-djpeg','-r600'); % resolution set to 600 dpg
end
     