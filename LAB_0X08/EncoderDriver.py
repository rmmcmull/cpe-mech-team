'''
@file EncoderDriver.py

@brief This code creates a EncoderDriver class to measure the movement of 
       the encoder angle with an optical encoder.

@author Ryan McMullen and David Meyenberg

@date March 17, 2021

'''
import pyb

class EncoderDriver:
	'''
	Allows the user to read angle measurements from the encoder.
	'''
	def __init__(self, encoderA_pin, encoderB_pin, timerNumber, prescaler, period):
		'''
		@brief Creates a EncoderDriver object to read an encoder.
		@param encoderA_pin A pyb.Pin object used as the encoder's A pin.
		@param encoderB_pin A pyb.Pin object used as the encoder's B pin.
		@param timerNumber A value used to create a pyb.Timer object to count for the encoder.
		@param prescaler A value used for the timer.
		@param period A value used for the timer.
		'''
		## Encoder A pin
		self.EncoderA_pin = encoderA_pin
		## Encoder B pin
		self.EncoderB_pin = encoderB_pin
		## Encoder timer number
		self.TimerNumber = timerNumber
		## Encoder timer prescaler
		self.Prescaler = prescaler
		## Encoder timer period
		self.Period = period
		
		#Creates a pyb.Timer object using the timing number, prescaler, and period provided
		## Encoder timer object
		self.Timer = pyb.Timer(self.TimerNumber, prescaler = self.Prescaler, period = self.Period)
		self.Timer.channel(1, self.Timer.ENC_A, pin = self.EncoderA_pin)
		self.Timer.channel(2, self.Timer.ENC_B, pin = self.EncoderB_pin)
		
		#Stores timer counts
		## Encoder timer current value
		self.CurCount = 0
		## Encoder timer previous value
		self.PrevCount = 0
		
		#Stores position and change in position
		## Encoder position
		self.Position = 0
		## Encoder change in position
		self.Delta = 0
		

	def update(self):
		'''
		@brief Updates the position of the encoder and calculates the amount of change in position to store in delta. If theres is overflow or underflow, the delta is adjusted. The delta value is added to the current position.
		'''
		self.CurCount = self.Timer.counter()

		self.Delta = self.CurCount - self.PrevCount
		#Overflow: Reduces delta by a period
		if(self.Delta > (self.Period/2)):
			adjustedDelta = self.Delta - self.Period;
			self.Position += adjustedDelta;
		#Underflow: Increases delta by a period
		elif(self.Delta < (-self.Period/2)):
			adjustedDelta = self.Delta + self.Period;
			self.Position += adjustedDelta
		#Normal case: No delta adjustment
		else:
			self.Position += self.Delta
		self.PrevCount = self.CurCount

	def get_position(self):
		'''
		@brief Returns the most recently updated position of the encoder.
		'''
		return self.Position

	def set_position(self, position):
		'''
		@brief Sets the position of the encoder.
		'''
		self.Position = position
		self.CurCount = position
		self.PrevCount = position

	def get_delta(self):
		'''
		@brief Returns the amount of change in position since the last update.
		'''
		return self.Delta

	def zero(self):
		'''
		@brief Sets the position of the encoder to 0.
		'''
		self.set_position(0)