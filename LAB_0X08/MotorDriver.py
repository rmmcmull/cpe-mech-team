'''
@file MotorDriver.py

@brief This code includes a Python class, MotorDriver, which is used to 
         encapsulate all of the functionality that will be useful for interacting
         with the motor and to write test code evaluating the functionality
         of the motor driver class. For this class, you are able to to create
         two objects that can independently control two seperate motor drivers.
         The motors can be controlled to spin either forward or reverse and PWM
         can be used to control the speed of either motor.
         In addition to this, a fault tolerance is implemented which triggers
         an external interrupt callback when there is a fault.

@author Ryan McMullen and David Meyenberg

@date March 11, 2021
'''

import pyb, utime

class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board.'''
    
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer,ch1,ch2):
        '''  
		@brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param timer        A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        @param ch1			A value to select the channel on the timer
        @param ch2			A value to select the channel on the timer
        '''
        
        ## init pin connected to nsleep DRV8847 Pin as output
        self.nSLEEP_pin = nSLEEP_pin
        
        ## init DRV8847 Pin IN1 which will be used to drive the motor in either a forward or reverse direction
        self.IN1_pin = IN1_pin
        
        ## init DRV8847 Pin IN1 which will be used to drive the motor in either a forward or reverse direction
        self.IN2_pin = IN2_pin
        
        ## init timer (fill in once you better understand this oned)
        self.timer = timer
        
        ## specifies which channel to use for timer
        self.ch1 = ch1
        
        ## specifies which channel to use for timer
        self.ch2 = ch2
        
        ## init timer 3 channel 1
        self.t3ch1 = timer.channel(self.ch1,pyb.Timer.PWM, pin=self.IN1_pin)
        
        ## init timer 3 channel 2
        self.t3ch2 = timer.channel(self.ch2,pyb.Timer.PWM, pin=self.IN2_pin)

        ## flag to indicate nFault
        self.nFault = False

        ## interrupt that is called when the nfault pin is high
        self.FaultInt = None

    def enable(self):
        ''' 
		@brief method is called when the user desires to enable the motor. This sets the output of nsleep DRV8847 Pin to 1 (high).
        '''
        if(self.FaultInt != None):
            self.FaultInt.disable()
        self.nSLEEP_pin.high()
        utime.sleep_ms(1)
        if(self.FaultInt != None):
            self.FaultInt.enable()

        print('Enabling Motor')
        
    def disable (self):
        ''' 
		@brief method is called when the user desires to disable the motor. This sets the output of nsleep DRV8847 Pin to 0 (low).
        '''
        
        self.nSLEEP_pin.low()
        
        print('Disabling Motor')
        
    def set_duty(self, duty):
        '''
        @brief This method sets the duty cycle to be sent to the motor to the given level. Positive values cause effort in one direction, negative values in the opposite direction.
        @param duty A signed integer holding the duty cycle of the PWM signal sent to the motor
        '''
        
        self.duty = duty
        
        # if the duty cycle is positive, the motor needs to be driven in the 
        # forward direction.
        if self.duty > 0:
            
            ## DRV8847 Pin IN1 goes "high" (with associated duty cycle)
            self.t3ch1.pulse_width_percent(self.duty)
            
            ## DRV8847 Pin IN2 goes low
            self.t3ch2.pulse_width_percent(0)
            
        # if the duty cycle is negative, the motor needs to be driven in the 
        # reverse direction.
        elif self.duty < 0:
            
            # DRV8847 Pin IN2 goes "high" (with associated duty cycle)
            self.t3ch2.pulse_width_percent(abs(self.duty))
            
            # DRV8847 Pin IN1 goes low
            self.t3ch1.pulse_width_percent(0)
        
        # if duty cycle of 0 inputted, need to stop the motor
        elif self.duty == 0:
            # DRV8847 Pin IN2 goes low
            self.t3ch2.pulse_width_percent(0)
            
            # DRV8847 Pin IN1 goes low
            self.t3ch1.pulse_width_percent(0)
        else:
            pass
