'''
@file main.py

@brief This file combines the Encoder, Motor, and TouchPanel drivers we created for previous labs with the kinematic 
and dynamic analysis of the platform from previous labs to balance a ball on the platform. First we initialize the touchpad with device specific
values. Then, we initialzie the two encoders and two motors. We also create an interrupt and interrupt service routine to prevent any mechanical faults
from causing damage. Lastly, we continuously run the two motor operations, which are similar. The motor operations calculate a period, reads from
the touch panel to find position and velocity, and reads from the encoder to find angular position and velocity. It then applies the gains to the
values found to find the motor torque. Lastly, it converts the motor torque to a duty cycle using the gear ratio and the motor torque 
conversion factor, which is then set as the motor's duty cycle.

@author  Ryan McMullen and David Meyenberg

@date March 17, 2021
'''

import pyb
import utime
import math
from EncoderDriver import EncoderDriver
from MotorDriver import MotorDriver
from TouchPanelDriver import touchy


if __name__ == "__main__":
	
	# TOUCH PANEL INITIALIZATION*********************************************************
	## Initialize touch Panel and corresponding pins
	TPD = touchy(pyb.Pin.board.PA7, pyb.Pin.board.PA1, pyb.Pin.board.PA6, pyb.Pin.board.PA0, 0.959, 3.937, 3.480, 1.979)
	## x position of ball on platform
	x = 0
	## x velocity of ball on platform
	x_dot = 0
	## y position of ball on platform
	y = 0
	## y velocity of ball on platfrom
	y_dot = 0
	# ***********************************************************************************

	# ENCODERS INITIALIZATION************************************************************
	## Encoder 1 pin
	Encoder1APin = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
	## Encoder 1 pin
	Encoder1BPin = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)
	##Encoder 2 pin
	Encoder2APin = pyb.Pin(pyb.Pin.cpu.C6, pyb.Pin.IN)
	##Encoder 2 pin
	Encoder2BPin = pyb.Pin(pyb.Pin.cpu.C7, pyb.Pin.IN)
	## Encoder 1 timer number
	TimerNum1 = 4
	## Encoder 2 timer number
	TimerNum2 = 8
	## Timer period that both encoders will use
	Period = 65535
	## Timer prescaler that both encoders will use
	Prescaler = 0
	## Encoder 1 object 
	Encoder1 = EncoderDriver(Encoder1APin, Encoder1BPin, TimerNum1, Prescaler, Period)
	## Encoder 2 object
	Encoder2 = EncoderDriver(Encoder2APin, Encoder2BPin, TimerNum2, Prescaler, Period)
	# set encoders initial position
	Encoder1.zero()
	Encoder2.zero()	
	## Angular position about x axis
	theta_x = 0
	## Angular velocity about x axis
	theta_dot_x = 0
	## Angular position about y axis
	theta_y = 0
	## Angular velocity about y axis
	theta_dot_y = 0
	# ***********************************************************************************

	# MOTORS INITIALIZATION**************************************************************
	## CPU pin which is connected to DRV8847 nSLEEP pin 
	pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
	## CPU pin which is connected to DRV8847 IN1 pin
	pin_IN1    = pyb.Pin.board.PB4;
	## CPU pin which is connected to DRV8847 IN2 pin
	pin_IN2    = pyb.Pin.board.PB5;
	## CPU pin which is connected to DRV8847 IN3 pin
	pin_IN3    = pyb.Pin.board.PB0;
	## CPU pin which is connected to DRV8847 IN4 pin
	pin_IN4    = pyb.Pin.board.PB1;
	## Timer object used for PWM generation   
	time = pyb.Timer(3,freq = 20000);
	## Motor 1 object
	Motor1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, time, 1, 2)
	## Motor 2 object
	Motor2 = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, time, 3, 4)
	# Enable the motor driver for both motors
	Motor1.enable()
	Motor2.enable()
	        
	def nFaultISR(self, pin):
		'''
		@brief Interrupt function that is called when the nFault is detected on either motor. 
		Disables both motors and sets nFault to true, so the user can fix the hardware issue.
		'''
		Motor1.disable()
		Motor2.disable()
		## Motor 1 nFault flag
		Motor1.nFault = True
		## Motor 2 nFault flag
		Motor2.nFault = True

    ## External interrupt on nFault pin
	nFaultInt = pyb.ExtInt(pyb.Pin.cpu.B2, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, nFaultISR)
	# set interrupt to both motor drivers so they can 
	Motor1.FaultInt = nFaultInt
	Motor2.FaultInt = nFaultInt

	
	# Assign proportional gain values
	## Gain for velocity
	K1 = -3.0	
	## Gain for angular velocity
	K2 = -.075  
	## Gain for position
	K3 = -30
	## Gain for angular position
	K4 = -4.2	
	
	## Conversion factor from motor torque to duty cycle (K_duty = R/(Vdc * Kt) = 2.21 ohms /(12V * 12.8 mN/A))
	K_duty = 13.3454	
	
	## Gear ratio between motor and encoder (12 teeth on motor gear and 48 teeth on encoder gear; 48/12 = 4)
	GearRatio = 4

	# ***********************************************************************************
	
#	CONTROL OPERATION *******************************************************************
	# set initial time equal to zero for velocity calculations
	## Placeholder for last time1
	old_time1 = 0
	## Placeholder for last time2
	old_time2 = 0
	while(True):
		
# 		# MOTOR 1 OPERATION *************************************************************
		
		# Time Calculations
		## Most recent time1
		new_time1 = utime.ticks_us() / 1000000
		## Time since last call that is used as a period for velocity calucations
		period1 = new_time1 - old_time1
		# sets old time to most recent time for next call
		old_time1 = new_time1
		
		# ensures the touch panel is actually being touched when reading values
		if(TPD.z_read() == True):
			## Y-axis reading in meters
			y_new = TPD.y_read() / 1000		
			# sets to y-axis velocity by calculating the change in position since last call
			y_dot = (y_new - y) / period1			
			# sets y to most recent y reading for next call
			y = y_new
	
		# update encoder 1 before reading position
		Encoder1.update()
		## Encoder 1 angular position in radians
		theta_x_new = Encoder1.get_position() * (math.pi / 1000)
		# sets to angular velocity by calculating change in angular positin since last call
		theta_dot_x = (theta_x_new - theta_x) / period1
		# sets theta_x to most recent theta_x reading for next call
		theta_x = theta_x_new

		# applies gains to state space variables to find motor torque
		## Torque value for motor 1 
		Torque_A = ((-K1 * y_dot) - (K2 * theta_dot_x) - (K3 * y) - (K4 * -theta_x))
		# applies the gear ratio and converts torque to duty cycle
		## Duty cycle for motor 1
		Duty_Cycle_A = int(K_duty * Torque_A * GearRatio)
		# runs motor 1 at calculated duty cycle
		Motor1.set_duty(Duty_Cycle_A)
		# checks for motor fault and waits for user to fix before enabling
		if(Motor1.nFault == True):
			input('Motor Fault! Press enter to resume')
			Motor1.enable()
			Motor1.nFault = False

# 		# *******************************************************************************
# 		
# 		# MOTOR 2 OPERATION *************************************************************
		
		# Time Calculations
		## Most recent time2
		new_time2 = utime.ticks_us() / 1000000
		## Time since last call that is used as a period for velocity calucations
		period2 = new_time2 - old_time2
		# sets old time to most recent time for next call
		old_time2 = new_time2

		# ensures the touch panel is actually being touched when reading values
		if(TPD.z_read() == True):
			## X-axis reading in meters
			x_new = TPD.x_read() / 1000		
			# sets to x-axis velocity by calculating the change in position since last call
			x_dot = (x_new - x) / period2	
			# sets x to most recent x reading for next call
			x = x_new
		# update encoder 2 before reading position
		Encoder2.update()
		## Encoder 2 angular position in radians
		theta_y_new = Encoder2.get_position() * (math.pi / 1000)
		# sets to angular velocity by calculating change in angular positin since last call
		theta_dot_y = (theta_y_new - theta_y) / period2
		# sets theta_x to most recent theta_x reading for next call
		theta_y = theta_y_new

		# applies gains to state space variables to find motor torque
		## Torque value for motor 2 
		Torque_B = ((-K1 * x_dot) - (K2 * theta_dot_y) - (K3 * x) - (K4 * -theta_y))
		# applies the gear ratio and converts torque to duty cycle
		## Duty cycle for motor 2
		Duty_Cycle_B = int(K_duty * Torque_B * GearRatio)
		# runs motor 1 at calculated duty cycle
		Motor2.set_duty(Duty_Cycle_B)
		# checks for motor fault and waits for user to fix before enabling
		if(Motor2.nFault == True):
			input('Motor Fault! Press enter to resume')
			Motor2.enable()
			Motor2.nFault = False

# 		# *******************************************************************************